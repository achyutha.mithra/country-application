package com.example.demo.controller;

import com.example.demo.model.Country;
import com.example.demo.model.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.lang.module.Configuration;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

@RestController
public class DemoController {

    @Autowired
    CountryRepository countryRepository;

    @GetMapping("/countries")
    public List<Country> fetchCountries(){
        return countryRepository.findAll();
    }

    @GetMapping("/countries/{id}")
    public Country fetchCountryById(@PathVariable("id") Integer countryId){
        return countryRepository.findById(countryId).orElseThrow(()->new ResponseStatusException(HttpStatus.BAD_REQUEST,"no.country.present.with.matching.id"));
    }

    @GetMapping("/countries/alpha/{code}")
    public Country fetchCountry(@PathVariable("code") String code){
        //1. Functional and normal approach

        /**
         * 1. Iterate the list,
         * 2. get its Alpha and compare with code,
         * 3. if a match exists, return that object.
         * 4. else, after iteration, no match is found, return null.
         */


//        for(int i=0; i<countryList.size();i++){
//            if(countryList.get(i).getAlpha().equals(code))
//                return countryList.get(i);
//        }

        // Java8
//        var fetchedCountry = countryList
//                .stream()
//                .filter(country-> country.getAlpha().equals(code))
//                .findFirst();
//
//        if(fetchedCountry.isPresent())
//            return fetchedCountry.get();

//        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "no.such.country");
        return null;
    }

    @PostMapping("/countries")
    public Country addNewCountry(@RequestBody Country country){
        System.out.println(country);
        /*
        1. length of the list
        2. functional
         */

//        country.setId(countryList.size()+1);
//        countryList.add(country);
//
//        var maxId = countryList
//                .stream()
//                .sorted(Comparator.comparing(Country::getId).reversed())
//                .map(country1-> {
//                    return country1.getId();
//                })
//                .findFirst().get();
//
//        country.setId(maxId);

        return countryRepository.save(country);
    }

    /*
    i. Get, POST, PUT and DELETE
    1. GET - used to fetch something/resource from the backend(or the application)
    2. POST - used to create a new resource in the backend.
     */


}
